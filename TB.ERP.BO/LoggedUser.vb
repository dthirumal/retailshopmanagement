﻿

<Serializable()>
Public Class LoggedUser


    Private _RoleID As Integer
    Private _RoleName As String
    Private _Name As String
    Private _MillitaryNo As Integer
    Private _MenuDetails As DataTable
    Private _Lang As String
    Private _AccessView As Boolean
    Private _AccessPrint As Boolean
    Private _AccessAdd As Boolean
    Private _AccessEdit As Boolean
    Private _AccessDelete As Boolean
    Private _RankCode As Integer
    Private _RankDescription As String

    Public Property RankDescription() As String
        Get
            Return _RankDescription
        End Get
        Set(ByVal value As String)
            _RankDescription = value
        End Set
    End Property


    Public Property RankCode() As Integer
        Get
            Return _RankCode
        End Get
        Set(ByVal value As Integer)
            _RankCode = value
        End Set
    End Property


    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property
    Public Property AccessView() As Boolean
        Get
            Return _AccessView
        End Get
        Set(ByVal value As Boolean)
            _AccessView = value
        End Set
    End Property

    Public Property AccessPrint() As Boolean
        Get
            Return _AccessPrint
        End Get
        Set(ByVal value As Boolean)
            _AccessPrint = value
        End Set
    End Property

    Public Property AccessAdd() As Boolean
        Get
            Return _AccessAdd
        End Get
        Set(ByVal value As Boolean)
            _AccessAdd = value
        End Set
    End Property

    Public Property AccessEdit() As Boolean
        Get
            Return _AccessEdit
        End Get
        Set(ByVal value As Boolean)
            _AccessEdit = value
        End Set
    End Property

    Public Property AccessDelete() As Boolean
        Get
            Return _AccessDelete
        End Get
        Set(ByVal value As Boolean)
            _AccessDelete = value
        End Set
    End Property

    Public Property Lang() As String
        Get
            Return _Lang
        End Get
        Set(ByVal value As String)
            _Lang = value
        End Set
    End Property

    Public Property MenuDetails() As DataTable
        Get
            Return _MenuDetails
        End Get
        Set(ByVal value As DataTable)
            _MenuDetails = value
        End Set
    End Property

    Public Property MillitaryNo() As Integer
        Get
            Return _MillitaryNo
        End Get
        Set(ByVal value As Integer)
            _MillitaryNo = value
        End Set
    End Property


    Public Property RoleName() As String
        Get
            Return _RoleName
        End Get
        Set(ByVal value As String)
            _RoleName = value
        End Set
    End Property

    Public Property RoleId() As Integer
        Get
            Return _RoleID
        End Get
        Set(ByVal value As Integer)
            _RoleID = value
        End Set
    End Property


End Class
