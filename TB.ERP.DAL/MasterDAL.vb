﻿Imports TB.ERP.BO
Imports System.Data.SqlClient

Public Class MasterDAL
    Inherits SqlHelper
    Public Shared Function GetUserDetails(ByVal MillitaryNo As Integer, ByVal Lang As String) As DataRow
        Dim CMD As New SqlCommand("dbo.GetUserDetails")
        CMD.Parameters.Add("@MillitaryNo", SqlDbType.Int).Value = MillitaryNo
        CMD.Parameters.Add("@Lang", SqlDbType.VarChar).Value = Lang
        Return ExecuteCMDOnCFD(CMD).Tables(0).Rows(0)
    End Function
End Class
