﻿''Author : Ammar Khalil
''Description : Connection Adapter to Generate DB Connections (Singleton)
''Dated : 10-July-2012

Imports System.Configuration
Imports System.Data.SqlClient



Public NotInheritable Class ConnectionAdapter
    Private Sub New()
    End Sub 'New

    Private Shared _conn As SqlConnection = Nothing
    Public Shared ReadOnly Property CFDConnectionString()
        Get
            Return ConfigurationManager.ConnectionStrings(DALConstans.__CONNECTION_STRING_CFD).ToString
        End Get
    End Property
    Public Shared ReadOnly Property SOCConnectionString()
        Get
            Return ConfigurationManager.ConnectionStrings(DALConstans.__CONNECTION_STRING_SOC).ToString
        End Get
    End Property

    Public Shared ReadOnly Property sqlConCFD()
        Get
            _conn = New SqlConnection(CFDConnectionString)
            Return _conn
        End Get
    End Property

    Public Shared ReadOnly Property sqlConSOC()
        Get
            _conn = New SqlConnection(SOCConnectionString)
            Return _conn
        End Get
    End Property

End Class