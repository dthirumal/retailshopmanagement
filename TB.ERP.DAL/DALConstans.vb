﻿''Author : Ammar Khalil
''Constants Class for Data Access Layer
''Dated : 11-July-2012

Public NotInheritable Class DALConstans
    Private Sub New()
    End Sub 'New
    '' Seprators
    Public Const __SEPERATOR As String = "/"

    ''Operation Type
    Public Const __OP_TYPE_INSERT As Integer = 1
    Public Const __OP_TYPE_UPDATE As Integer = 2

    ''PO History Type
    Public Const __PO_HISTORY_TYPE_AMENDMENT As Integer = 1
    Public Const __PO_HISTORY_TYPE_CFD_HEAD_REJECTS_AMENDMENT As Integer = 2


    ''Database Info
    Public Const __CONNECTION_STRING_SOC As String = "MainDBHR"
    Public Const __CONNECTION_STRING_CFD As String = "CFDConnectionString"

    ''Schema Names
    Public Const __SCHEMA_DBO As String = "dbo"
    Public Const __SCHEMA_PURCHASE As String = "Purchase"
    Public Const __SCHEMA_FINANCE As String = "Finance"
    Public Const __SCHEMA_GENERICS As String = "Generics"
    Public Const __SCHEMA_INVENTORY As String = "Inventory"
    Public Const __SCHEMA_WORKFLOW As String = "Workflow"
    Public Const __SCHEMA_ACCESS As String = "Access"
    Public Const __SCHEMA_COMPANY As String = "Company"
    Public Const __SCHEMA_ATTACHMENTS As String = "Attachments"
    Public Const __SCHEMA_CONSTRUCTION As String = "Construction"
    ''Procedure Names

    ''Generic
    Public Const __SP_GETSTATICDATA As String = __SCHEMA_GENERICS + "." + "GetStaticData"
    Public Const __SP_GENERATESERIAL As String = __SCHEMA_GENERICS + "." + "GenerateSerial"

    ''Serial Genrators -ATTRIBUTE VALUE
    Public Const __SERIALGENERATOR_PO As String = "PO"
    Public Const __SERIALGENERATOR_COMPANY As String = "COMPANY"
    Public Const __SERIALGENERATOR_ATTACHMENTS As String = "ATTACHMENT_ID"
    Public Const __SERIALGENERATOR_ATTACHMENT_REF_NO As String = "ATTACHMENT_REF_NO"
    Public Const __SERIALGENERATOR_PGFILE As String = "PGFILE"
    Public Const __SERIALGENERATOR_GENERAL_RECEIPT As String = "GENERAL_RECEIPT"
    Public Const __SERIALGENERATOR_COMMITTEE As String = "COMMITTEE"
    Public Const __SERIALGENERATOR_FINANCE As String = "FINANCE"
    Public Const __SERIALGENERATOR_TENDERS As String = "TENDERS"

    '''' Purchase Order
    Public Const __SP_SAVEUPDATE_PO As String = __SCHEMA_PURCHASE + "." + "PO_SaveUpdate"
    Public Const __SP_GETPOAGAINSTID As String = __SCHEMA_PURCHASE + "." + "PO_GETPO"
    Public Const __SP_GETALLPO As String = __SCHEMA_PURCHASE + "." + "PO_GETALL"
    Public Const __SP_GETALLPOAMENDMENTS As String = __SCHEMA_PURCHASE + "." + "PO_GetAllAmended"
    Public Const __SP_SAVEUPDATE_POLINEITEMS As String = __SCHEMA_PURCHASE + "." + "POLine_SaveUpdate"
    Public Const __SP_SAVEUPDATE_POTERMS As String = __SCHEMA_PURCHASE + "." + "POTerms_SaveUpdate"
    Public Const __SP_DEL_POLINEITEMS As String = __SCHEMA_PURCHASE + "." + "POLine_DELItems"
    Public Const __SP_POAMENDMENT As String = __SCHEMA_PURCHASE + "." + "PO_Amendment"


    Public Const __SP_GETUNITS As String = "Units_GetDetails" ''From SOCDB
    Public Const __SP_GETUNITSINXML As String = __SCHEMA_GENERICS + "." + "GetCFDUnitsXMLnLevels" ''From SOCDB

    '''' Documents & Attachments
    Public Const __SP_ATTACHMENTS_SAVE As String = __SCHEMA_ATTACHMENTS + "." + "Attachments_Save"
    Public Const __SP_GETATTACHMENTS As String = __SCHEMA_ATTACHMENTS + "." + "GetAttachments"
    Public Const __SP_VIEWATTACHMENT As String = __SCHEMA_ATTACHMENTS + "." + "ViewAttachment"
    Public Const __SP_DELETEATTACHMENT As String = __SCHEMA_ATTACHMENTS + "." + "DeleteAttachment"
    Public Const __SP_UPDATEATTACHMENTINFO As String = __SCHEMA_ATTACHMENTS + "." + "Attachments_UpdateInfo"


    '''' Workflow
    Public Const __SP_GETWORKFLOW As String = __SCHEMA_WORKFLOW + "." + "WF_GetDetails"
    Public Const __SP_GETWORKFLOWSTATUSLOG As String = __SCHEMA_WORKFLOW + "." + "WF_GetStatusLog"
    Public Const __SP_GETWORKFLOWSTATUSBYMODULE As String = __SCHEMA_WORKFLOW + "." + "WFStatus_Select"

    '''' Access And User Details
    Public Const __SP_GETACCESS As String = __SCHEMA_ACCESS + "." + "GetAccess"
    Public Const __SP_GETUSERDETAILS As String = __SCHEMA_ACCESS + "." + "GetUserDetails"
    Public Const __SP_GETLOCATIONS As String = __SCHEMA_GENERICS + "." + "GetLocations"

    '''' Company
    Public Const __SP_GETCOMPANYBYID As String = __SCHEMA_GENERICS + "." + "CompanyByID_Select"
    Public Const __SP_GETALLCOMPANY As String = __SCHEMA_GENERICS + "." + "Company_ListAll"
    Public Const __SP_SAVEUPDATE_COMPANY As String = __SCHEMA_COMPANY + "." + "Company_SaveUpdate"
    Public Const __SP_SAVEUPDATE_COMPANYADDRESS As String = __SCHEMA_COMPANY + "." + "CompanyAddress_SaveUpdate"
    Public Const __SP_SAVEUPDATE_COMPANYEMPLOYEES As String = __SCHEMA_COMPANY + "." + "CompanyEmployees_SaveUpdate"
    Public Const __SP_SAVEUPDATE_COMPANYDOCUMENTS As String = __SCHEMA_COMPANY + "." + "CompanyDocuments_SaveUpdate"
    Public Const __SP_UPDATE_COMPANYVOUCHERS As String = __SCHEMA_COMPANY + "." + "CompanyVouchers_Update"
    Public Const __SP_GETALLCOMPANIES As String = __SCHEMA_COMPANY + "." + "Company_GetAll"
    Public Const __SP_GETCOMPANY As String = __SCHEMA_COMPANY + "." + "Company_GetCompany"
    Public Const __SP_SAVEUPDATE_REVENUERECEIPT As String = __SCHEMA_COMPANY + "." + "RevenueReceipt_SaveUpdate "
    Public Const __SP_SAVEUPDATE_REVENUERECEIPTVOUCHERS As String = __SCHEMA_COMPANY + "." + "RevenueVouchers_SaveUpdate"
    Public Const __SP_GETALLREVENUERECEIPTS As String = __SCHEMA_COMPANY + "." + "RevenueReceipt_GetAll"
    Public Const __SP_GETREVENURECEIPT As String = __SCHEMA_COMPANY + "." + "RevenueReceipt_GETRR"
    Public Const __SP_UPDATEGRRADFINANCEDETAILS As String = __SCHEMA_COMPANY + "." + "RevenueReceipt_UpdateADFinDetails"

    '''' Construction
    Public Const __SP_TENDERSPERFROMACTIONS As String = __SCHEMA_CONSTRUCTION + "." + "Tenders_PerformActions"
    Public Const __SP_TENDERS_INSERTUPDATE As String = __SCHEMA_CONSTRUCTION + "." + "Tenders_InsertUpdate"
    Public Const __SP_TENDERCOMPANIES_INSERTUPDATE As String = __SCHEMA_CONSTRUCTION + "." + "TenderCompanies_InsertUpdate"
    Public Const __SP_TENDERCOMMITTEE_INSERTUPDATE As String = __SCHEMA_CONSTRUCTION + "." + "TenderCommittee_InsertUpdate"
    Public Const __SP_TENDERUNITS_INSERTUPDATE As String = __SCHEMA_CONSTRUCTION + "." + "TenderUnits_InsertUpdate"
    Public Const __SP_UNITLOCATIONS_INSERTUPDATE As String = __SCHEMA_CONSTRUCTION + "." + "UnitLocations_InsertUpdate"
    Public Const __SP_EXTENDPERIOD_INSERTUPDATE As String = __SCHEMA_CONSTRUCTION + "." + "ExtendPeriod_InsertUpdate"

    ''''Inventory
    Public Const __SP_ITEMMASTER_PERFROMACTIONS As String = __SCHEMA_INVENTORY + "." + "ItemMaster_PerformActions"
    Public Const __SP_CATEGORY_INSERTUPDATE As String = __SCHEMA_INVENTORY + "." + "Category_InsertUpdate"
    Public Const __SP_SUBCATEGORY_INSERTUPDATE As String = __SCHEMA_INVENTORY + "." + "Subcategory_InsertUpdate"
    Public Const __SP_CATEGORYATTRIBUTES_INSERTUPDATE As String = __SCHEMA_INVENTORY + "." + "CategoryAttributes_InsertUpdate"
    Public Const __SP_ATTRIBUTEMASTER_INSERTUPDATE As String = __SCHEMA_INVENTORY + "." + "AttributeMaster_InsertUpdate"
    Public Const __SP_BRANDS_INSERTUPDATE As String = __SCHEMA_INVENTORY + "." + "Brands_InsertUpdate"
    Public Const __SP_BRANDMODELS_INSERTUPDATE As String = __SCHEMA_INVENTORY + "." + "BrandModels_InsertUpdate"

    '''' Finace & Budgetting
    ' Public Const __SP_GETALLBUDGET As String = __SCHEMA_FINANCE + "." + "Budget_SelectAll"
    Public Const __SP_GETALLBUDGET As String = __SCHEMA_FINANCE + "." + "Budget_GetAll"
    Public Const __SP_SAVEUPDATE_ADFBUDGET As String = __SCHEMA_FINANCE + "." + "ADFBudget_SaveUpdate"
    Public Const __SP_SAVEUPDATE_ACCOUNTHEADS As String = __SCHEMA_FINANCE + "." + "AccountHeads_SaveUpdate"
    Public Const __SP_GETBUDGET As String = __SCHEMA_FINANCE + "." + "Budget_GetBudget"
    Public Const __SP_GETALLBCODES As String = __SCHEMA_FINANCE + "." + "BCode_GetAll"
    Public Const __SP_GETLASTBUDGETUTILIZATION As String = __SCHEMA_FINANCE + "." + "CheckLastBudgetUtilization"
    Public Const __SP_GETTRANSACTIONHISTORY As String = __SCHEMA_FINANCE + "." + "Get_TransactionHistory"
    Public Const __SP_PROCESSTRANSACTIONS As String = __SCHEMA_FINANCE + "." + "ProcessTransactions"

    Public Const __SP_GETACCOUNTS As String = __SCHEMA_FINANCE + "." + "SelectAccountsDetails"
    Public Const __SP_GETACCOUNTSMAIN As String = __SCHEMA_FINANCE + "." + "SelectAccounts"

    '' Pending Transfers and Accounts
    Public Const __SP_PENDINGTRANSFERSPERFORMACTIONS = __SCHEMA_FINANCE + "." + "PendingTransfers_PerfomActions"
    Public Const __SP_PENDINGACCOUNTSPERFORMACTIONS = __SCHEMA_FINANCE + "." + "Accounts_PerformActions"

    ''Reporting Constants
    Public Const __SP_RPT_GETPODETAILS As String = __SCHEMA_PURCHASE + "." + "RPT_GetPODetails"
    Public Const __SP_RPT_GETPOAMENDMENTS As String = __SCHEMA_PURCHASE + "." + "RPT_GetPOAmendments"
    Public Const __SP_RPT_GETCOMPANYDETAILS As String = __SCHEMA_COMPANY + "." + "RPT_GetCompanyDetails"
    Public Const __SP_RPT_GETGRRDETAILS As String = __SCHEMA_COMPANY + "." + "RPT_GetGRRDetails"
    Public Const __SP_RPT_GETRVDETAILS As String = __SCHEMA_PURCHASE + "." + "RPT_GetRVDetails"
    Public Const __SP_RPT_GETQUOTATIONDETAILS As String = __SCHEMA_PURCHASE + "." + "RPT_QuotationReview"
    Public Const __SP_RPT_GETFINANCEREPORT As String = __SCHEMA_FINANCE + "." + "RPT_Finance"

    ''Table Names
    Public Const _TBL_ADFBUDGET As String = __SCHEMA_FINANCE + "." + "ADFBudget"
    Public Const _TBL_ACCOUNTHEADS As String = __SCHEMA_FINANCE + "." + "AccountHeads"
    Public Const _TBL_BASICMASTERS As String = __SCHEMA_GENERICS + "." + "BasicMasters"
    Public Const _TBL_BASECURRENCY As String = __SCHEMA_FINANCE + "." + "BaseCurrency"
    Public Const _TBL_EXCHANGERATE As String = __SCHEMA_FINANCE + "." + "ExchangeRate"
    Public Const _TBL_REVENUEVOUCHERS As String = __SCHEMA_COMPANY + "." + "RevenueVouchers"
    Public Const _TBL_COMPANY As String = __SCHEMA_COMPANY + "." + "Company"
    Public Const _TBL_UNITSCFD As String = __SCHEMA_GENERICS + "." + "UnitsCFD"
    Public Const _TBL_COMMITTEE As String = __SCHEMA_PURCHASE + "." + "Committee"
    Public Const _TBL_APPREGISTER As String = __SCHEMA_ACCESS + "." + "ApplicationRegister"
    Public Const _TBL_PENDINGTRANSFERS As String = __SCHEMA_FINANCE + "." + "PendingTransfers"
    Public Const _TBL_BCODE As String = __SCHEMA_FINANCE + "." + "BCode"

End Class
