﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Data.Common
Imports System.Configuration


Public Class SqlHelper
    Public Shared __CONNECTION_TIMEOUT As Integer = 300

    Private Shared Function GetConnection(ByVal DBName As String) As SqlConnection
        Dim connection As SqlConnection = Nothing
        Try
            If "CFD".Equals(DBName) Then
                connection = ConnectionAdapter.sqlConCFD
            ElseIf "SOC".Equals(DBName) Then
                connection = ConnectionAdapter.sqlConSOC
            End If

            If connection Is Nothing Then
                Throw New Exception("Database Connection Error: ")
            End If
        Catch ex As Exception
            Throw (New Exception("Database Error While connecting to database : " + DBName + ":" + ex.Message))
        End Try
        Return connection
    End Function

    ''This method will execute plain queries against the database
   

    ''This method is used to execute stored procedures which are supposed to 
    ''return a singular value.
    Public Shared Function ExecuteScalarCMDOnCFD(ByRef CMD As SqlCommand) As Object
        Dim obj As Object
        Try
            CMD.Connection = GetConnection("CFD")
            CMD.CommandType = CommandType.StoredProcedure
            If CMD.CommandText.Contains(" ") Then
                CMD.CommandType = CommandType.Text
            End If
            CMD.Connection.Open()
            Dim retVal As New SqlParameter("@Value", SqlDbType.BigInt)
            retVal.Direction = ParameterDirection.ReturnValue
            CMD.Parameters.Add(retVal)
            CMD.ExecuteScalar()
            obj = retVal.Value

        Catch sqlex As SqlException
            Throw New Exception("SQL Exception: " & sqlex.Message)
        Catch ex As Exception
            Throw New Exception("Database Error: " & ex.Message)
        Finally
            CloseConnection(CMD.Connection)
        End Try
        Return obj
    End Function

    ''This method method will execute the stored procedure on CFD database.
    Public Shared Function ExecuteCMDOnCFD(ByRef CMD As SqlCommand) As DataSet
        Dim ds As New DataSet()
        Try
            CMD.Connection = GetConnection("CFD")
            CMD.CommandType = CommandType.StoredProcedure
            If CMD.CommandText.Contains(" ") Then
                CMD.CommandType = CommandType.Text
            End If
            Dim adapter As New SqlDataAdapter(CMD)
            adapter.SelectCommand.CommandTimeout = __CONNECTION_TIMEOUT
            adapter.Fill(ds)
        Catch sqlex As SqlException
            Throw New Exception("SQL Exception: " & sqlex.Message)
        Catch ex As Exception
            Throw New Exception("Database Error: " & ex.Message)
        Finally
            CloseConnection(CMD.Connection)
        End Try
        Return ds
    End Function

    ''This method is used to execute stored procedure on SOC Db , mainly
    ''used to get the lookup data.
    Public Shared Function ExecuteCMDOnSOC(ByRef CMD As SqlCommand) As DataSet
        Dim ds As New DataSet()
        Try
            CMD.Connection = GetConnection("SOC")
            CMD.CommandType = CommandType.StoredProcedure
            If CMD.CommandText.Contains(" ") Then
                CMD.CommandType = CommandType.Text
            End If
            Dim adapter As New SqlDataAdapter(CMD)
            adapter.SelectCommand.CommandTimeout = 300
            adapter.Fill(ds)
        Catch sqlex As SqlException
            Throw New Exception("SQL Exception: " & sqlex.Message)
        Catch ex As Exception
            Throw New Exception("Database Error: " & ex.Message)
        Finally
            CloseConnection(CMD.Connection)
        End Try
        Return ds
    End Function

    ''This method is the only method to generate and return serial numbers, all child classes
    ''will call this method with the attribute name to get the desired serial.
    Public Shared Function GetReference(ByVal Attr As String, ByVal Year As Integer, ByVal Increment As Short) As Int64
        Dim CMD As New SqlCommand(DALConstans.__SP_GENERATESERIAL)
        Try
            CMD.Parameters.Add("@Attribute", SqlDbType.VarChar).Value = Attr.Trim
            CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year
            CMD.Parameters.Add("@Increment", SqlDbType.TinyInt).Value = Increment
            Return Convert.ToInt64(ExecuteScalarCMDOnCFD(CMD))
        Catch ex As SqlException
            Throw ex
        End Try
    End Function

    ''This method is used to get the value from the data row provided, based on the column name
    ''this centralized method will make sure that null reference errors should never be thrown,
    ''and if occured they are handled with proper return value
    Public Shared Function GetItemFromDataRow(ByVal ColName As String, ByVal objType As Type, ByVal row As DataRow) As Object
        Dim obj As Object = String.Empty
        Try
            If row.Item(ColName) Is Nothing Or IsDBNull(row.Item(ColName)) Then
                If objType = GetType(Date) Then
                    obj = Date.MinValue
                ElseIf objType = GetType(String) Then
                    obj = String.Empty
                ElseIf objType = GetType(Integer) Then
                    obj = 0
                End If
            Else
                obj = row.Item(ColName)
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
        Return obj
    End Function

    Private Shared Sub CloseConnection(ByVal conn As SqlConnection)
        If conn IsNot Nothing Then
            conn.Close()
        End If
    End Sub

#Region "dbClass"
    Public Shared Function UploadPath() As String
        Dim UpLoadP As String = ConfigurationManager.AppSettings.Item("UploadPath").ToString()
        Return UpLoadP
    End Function

    Public Shared Function InsertSp(ByVal Parm As SqlParameter(), ByVal SpName As String) As Integer
        Dim Result As Integer = 0
        Dim _Conn As SqlClient.SqlConnection = ConnectionAdapter.sqlConCFD
        If _Conn.State = ConnectionState.Closed Then
            _Conn.Open()
        End If

        Dim _Tran As SqlClient.SqlTransaction
        _Tran = _Conn.BeginTransaction()
        Dim _Cmd As New SqlClient.SqlCommand(SpName, _Conn, _Tran)
        _Cmd.CommandType = CommandType.StoredProcedure

        For y As Integer = 0 To Parm.GetLength(0) - 1
            _Cmd.Parameters.Add(Parm(y))
        Next

        Try
            Result = _Cmd.ExecuteNonQuery()
        Catch ex As Exception
            _Tran.Rollback()
            _Tran.Dispose()
            If _Conn.State = ConnectionState.Open Then
                _Conn.Close()
                _Conn.Dispose()
            End If
            Throw ex
        End Try

        If _Conn.State = ConnectionState.Open Then
            _Tran.Commit()
            _Tran.Dispose()
            _Conn.Close()
            _Conn.Dispose()
        End If

        Return Result

    End Function

    Public Shared Function getData(ByVal Parm As SqlParameter(), ByVal SpName As String) As DataTable
        Dim _table As New DataTable
        Dim _Conn As SqlConnection = ConnectionAdapter.sqlConCFD
        If _Conn.State = ConnectionState.Closed Then
            _Conn.Open()
        End If

        Dim _Cmd As New SqlCommand()
        _Cmd.CommandType = CommandType.StoredProcedure

        If Not Parm Is Nothing Then
            Dim ss As Integer = Parm.GetLength(0)
            For y As Integer = 0 To Parm.GetLength(0) - 1
                _Cmd.Parameters.Add(Parm(y))
            Next
        End If

        _Cmd.CommandText = SpName
        _Cmd.Connection = _Conn
        Dim _Adapter As New SqlDataAdapter(_Cmd)
        _Adapter.Fill(_table)
        If _Conn.State = ConnectionState.Open Then
            _Conn.Close()
            _Conn.Dispose()

        End If
        Return _table

    End Function

    Public Shared Function getDataSet(ByVal Parm As SqlParameter(), ByVal SpName As String) As DataSet
        Dim _table As New DataSet
        Dim _Conn As SqlConnection = ConnectionAdapter.sqlConCFD
        If _Conn.State = ConnectionState.Closed Then
            _Conn.Open()
        End If

        Dim _Cmd As New SqlCommand()
        _Cmd.CommandType = CommandType.StoredProcedure

        If Not Parm Is Nothing Then
            Dim ss As Integer = Parm.GetLength(0)
            For y As Integer = 0 To Parm.GetLength(0) - 1
                _Cmd.Parameters.Add(Parm(y))
            Next
        End If

        _Cmd.CommandText = SpName
        _Cmd.Connection = _Conn
        Dim _Adapter As New SqlDataAdapter(_Cmd)
        _Adapter.Fill(_table)
        If _Conn.State = ConnectionState.Open Then
            _Conn.Close()
            _Conn.Dispose()

        End If
        Return _table
    End Function

    Public Shared Function InsertSp_Transaction(ByVal Parm As SqlParameter(), ByVal SpName As String) As Integer
        Dim _Conn As SqlClient.SqlConnection = ConnectionAdapter.sqlConCFD
        If _Conn.State = ConnectionState.Closed Then
            _Conn.Open()
        End If

        Dim _Tran As SqlClient.SqlTransaction
        _Tran = _Conn.BeginTransaction()
        Dim _Cmd As New SqlClient.SqlCommand(SpName, _Conn, _Tran)
        _Cmd.CommandType = CommandType.StoredProcedure

        For y As Integer = 0 To Parm.GetLength(0) - 1
            _Cmd.Parameters.Add(Parm(y))
        Next
        ' Add Return value parameter
        Dim PramReturnValue As SqlParameter
        PramReturnValue = New SqlParameter("@return_value", SqlDbType.Int)
        PramReturnValue.Direction = ParameterDirection.ReturnValue
        _Cmd.Parameters.Add(PramReturnValue)
        Try
            _Cmd.ExecuteNonQuery()
            If PramReturnValue.Value <> 0 Then
                _Tran.Rollback()
                _Tran.Dispose()
            End If
        Catch ex As Exception
            _Tran.Rollback()
            _Tran.Dispose()
            If _Conn.State = ConnectionState.Open Then
                _Conn.Close()
                _Conn.Dispose()
            End If
            Throw ex
        End Try
        If _Conn.State = ConnectionState.Open Then
            _Tran.Commit()
            _Tran.Dispose()
            _Conn.Close()
            _Conn.Dispose()
        End If
        Return PramReturnValue.Value
    End Function
#End Region
End Class
