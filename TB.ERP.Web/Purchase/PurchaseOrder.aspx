﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ERPContent.Master" CodeBehind="PurchaseOrder.aspx.vb" Inherits="TB.ERP.Web.PurchaseOrder" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadAjaxPanel ID="pnlPurchaseOrderHeader" runat="server" width="100%" LoadingPanelID="RadLoadingPanel1">

    </telerik:RadAjaxPanel>
    <telerik:RadAjaxLoadingPanel ID="RadLoadingPanel1" runat="server" BackgroundPosition="Center"></telerik:RadAjaxLoadingPanel>
</asp:Content>
