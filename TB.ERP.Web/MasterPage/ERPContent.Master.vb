﻿Imports PGBaseLibrary
Imports TB.ERP.DAL
Imports TB.ERP.BO
Imports System.Threading
Imports System.Globalization

Public Class ERPContent
    Inherits System.Web.UI.MasterPage
    Public User As LoggedUser

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Private Sub BindRadMenuCFD()
        User = New LoggedUser
        User.Lang = "en-US"
        User.MillitaryNo = "100007591"

        Session(Constants.__SESSION_KEY_LOGGEDUSER) = LoadUserDetails(User)

        'User = DirectCast(GetFromSession(Constants.__SESSION_KEY_LOGGEDUSER), LoggedUser)
        RadMenuERP.DataTextField = "Menu_Desc"
        RadMenuERP.DataNavigateUrlField = "Menu_URL"
        RadMenuERP.DataFieldID = "Menu_Code"
        RadMenuERP.DataFieldParentID = "Menu_Menu_Code"
        RadMenuERP.DataSource = String.Empty
        RadMenuERP.DataSource = DirectCast(Session(Constants.__SESSION_KEY_LOGGEDUSER), LoggedUser).MenuDetails
        RadMenuERP.DataBind()

    End Sub

    Private Sub FnLoadEmpDetails()
        Dim dsGetProfilePhoto_byMilitaryNo As DataSet
        dsGetProfilePhoto_byMilitaryNo = PGBaseLibrary.AccessManager.GetProfilePhoto_byMilitaryNo_All(User.MillitaryNo, "NORMAL", User.Lang, 101, Page.Theme)
        If dsGetProfilePhoto_byMilitaryNo.Tables.Count >= 1 Then
            If dsGetProfilePhoto_byMilitaryNo.Tables.Count >= 1 AndAlso dsGetProfilePhoto_byMilitaryNo.Tables(1).Rows.Count > 0 Then
                EmpImage.DataValue = dsGetProfilePhoto_byMilitaryNo.Tables(1).Rows(0)(0)
                EmpImage.DataBind()
            End If
        End If
        If dsGetProfilePhoto_byMilitaryNo.Tables.Count >= 2 AndAlso dsGetProfilePhoto_byMilitaryNo.Tables(2).Rows.Count > 0 Then
            TopUserName.Text = UtilManager.IFNullEMpty(dsGetProfilePhoto_byMilitaryNo.Tables(2).Rows(0)("Military_Name"))
            lblRole.Text = UtilManager.IFNullEMpty(dsGetProfilePhoto_byMilitaryNo.Tables(2).Rows(0)("RoleName"))
        End If
    End Sub
    Private Function LoadUserDetails(ByVal User As LoggedUser) As LoggedUser

        User.MenuDetails = AccessManager.GetMenuDetailsForUser(0, User.Lang, User.MillitaryNo)
        'Dim dr As DataRow = MasterDAL.GetUserDetails(User.MillitaryNo, User.Lang)
       
     
        Return User
    End Function

    Private Function GetInitialUserInformation() As LoggedUser
        Dim lUser As New LoggedUser()
        'Check if the session contains information about
        'currently logged in user
        If Session(Constants.__SESSION_KEY_MIL_NO) Is Nothing Then
            ' The session object has expired or the user is visiting for the first time

            ' Load the details about the user from the database
            Dim strWindowsLoginName As String = String.Empty
            Dim strMilitaryNo As Integer?

            If Request.IsAuthenticated Then
                strWindowsLoginName = Page.User.Identity.Name
            End If

            If strWindowsLoginName.IndexOf("\", StringComparison.Ordinal) >= 0 Then
                strWindowsLoginName = strWindowsLoginName.Split("\")(1)
            End If

            'Using db As New CFDDBDataContext()
            '    strMilitaryNo = db.GetMilitaryNumberForWindowsLogin(strWindowsLoginName)

            '    If Not strMilitaryNo.HasValue Then
            '        ' No valid military number returned for the given windows login
            '        Return Nothing
            '    End If

            'End Using

            Session(Constants.__SESSION_KEY_MIL_NO) = strMilitaryNo
        End If

        Session(Constants.__SESSION_KEY_LANG) = If(Request.Cookies(Constants.__SESSION_KEY_LANG) IsNot Nothing, Request.Cookies(Constants.__SESSION_KEY_LANG).Value, Constants.__LANG_ENG_US)

        lUser.MillitaryNo = Session(Constants.__SESSION_KEY_MIL_NO)
        lUser.Lang = Session(Constants.__SESSION_KEY_LANG)

        Return lUser
    End Function
End Class