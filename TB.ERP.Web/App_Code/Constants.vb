﻿Public Class Constants
    Private Sub New()
    End Sub 'New
    ''Operation Type
   
    Public Const __BARCODE_FONT_CODE39 As String = "C:\Windows\Fonts\ElfringBar39FreeHp.ttf"

    ''Culture
    Public Const __LANG_ENG_US As String = "en-US"
    Public Const __LANG_AR As String = "ar-AE"

    ''Access
    Public Const __ACCESS_VIEW As String = "VIEW"
    Public Const __ACCESS_PRINT As String = "PRINT"
    Public Const __ACCESS_ADD As String = "ADD"
    Public Const __ACCESS_EDIT As String = "EDIT"
    Public Const __ACCESS_DELETE As String = "DELETE"

    ''
    Public Const __SESSION_KEY_MIL_NO As String = "military_no"
    Public Const __SESSION_KEY_USER_ID As String = "UserId"
    Public Const __SESSION_KEY_ROLE_ID As String = "RoleId"
    Public Const __SESSION_KEY_LANG As String = "Lang"
    Public Const __SESSION_KEY_LOGGEDUSER As String = "LoggedUser"

   

End Class
