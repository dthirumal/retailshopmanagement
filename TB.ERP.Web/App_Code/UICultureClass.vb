﻿Imports System.Threading
Imports System.Globalization
Imports System.Web.UI
Public Class UICultureClass
    Inherits Page

    Protected Overrides Sub InitializeCulture()

        'If (Request.Cookies("lang") IsNot Nothing) Then
        '    Dim userSettings As String = Request.Cookies("lang").Value
        '    Session("lang") = userSettings
        'End If
        Session("EmployeeID") = 1
        If Session("lang") Is Nothing Then
            Session("lang") = "en-US"
        End If
        'If Session("Lang") = "ar-AE" Then
        Page.Theme = "DataWebControls"
        'Else
        'Page.Theme = "PGDefault_en"
        'End If
        Dim MyCulture As String = Session("lang")
        Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(MyCulture)
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(MyCulture)
        MyBase.InitializeCulture()
    End Sub
End Class
